package br.com.itau.agenda.objetos;

import java.util.ArrayList;
import java.util.List;

public class Agenda {
	
	private String dono;
	private List<Contato> contatos;
	
	public Agenda(String dono) {
		this.dono = dono;
	}

	public void setContatos(List<Contato> contatos) {
		this.contatos = contatos;
	}
	
	@Override
	public String toString() {
		String texto = "Agenda de " + dono + ": \n";
		
		for (Contato contato : contatos) {
			texto += contato + "\n";
		}
		
		return texto;
	}
	
}
