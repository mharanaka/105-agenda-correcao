package br.com.itau.agenda.construtures;

import java.util.ArrayList;
import java.util.List;

import br.com.itau.agenda.console.Console;
import br.com.itau.agenda.objetos.Contato;

public class ConstrutorContatos {
	
	public static List<Contato> construir() {
		List<Contato> contatos = new ArrayList<>();
		
		boolean deveCriarNovoContato = true;
		
		while(deveCriarNovoContato) {
			
			Console.imprimePerguntaNomeContato();
			String nomeContato = Console.lerString();
			
			Console.imprimePerguntaTelefoneContato();
			long telefoneContato = Console.lerLong();
			
			Contato contato = new Contato(nomeContato, telefoneContato);
			contatos.add(contato);
			
			Console.imprimePerguntaNovoContato();
			deveCriarNovoContato = Console.lerSimOuNao();
		}
		
		return contatos;
	}

}
