package br.com.itau.agenda;

import java.util.List;

import br.com.itau.agenda.console.Console;
import br.com.itau.agenda.construtures.ConstrutorAgenda;
import br.com.itau.agenda.construtures.ConstrutorContatos;
import br.com.itau.agenda.objetos.Agenda;
import br.com.itau.agenda.objetos.Contato;

public class App {

	public static void main(String[] args) {
		Agenda agenda = ConstrutorAgenda.criar();
		
		List<Contato> contatos = ConstrutorContatos.construir();
		
		agenda.setContatos(contatos);
		
		Console.imprimeAgenda(agenda);
	}

}
